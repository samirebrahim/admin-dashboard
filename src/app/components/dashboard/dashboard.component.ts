import { Component, OnInit } from '@angular/core';
import { DataFactoryService } from '../../services/data-factory.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent  {

  data;
  constructor(private dataFactory: DataFactoryService) {
    this.dataFactory.getConfig().subscribe((data) => {
      this.data = data;
    });
  }



}
