import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataFactoryService {

  constructor(private http: HttpClient) { }

  getConfig() {
    return this.http.get(`http://ec2-18-216-18-250.us-east-2.compute.amazonaws.com:8082/cashi/api/analytics`);
  }

}
